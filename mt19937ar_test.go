/* Mersenne Twister 移植
 * Leonardone @ NEETSDKASU
 * BSD-2-Clause License
 */

package mersenne_twister_go

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"testing"
)

func TestMersenneTwister(t *testing.T) {

	var b bytes.Buffer

	initKey := []uint32{0x123, 0x234, 0x345, 0x456}

	var mt MersenneTwister

	mt.InitByArray(initKey)

	fmt.Fprintln(&b, "1000 outputs of genrand_int32()")
	for i := 0; i < 1000; i++ {
		fmt.Fprintf(&b, "%10d ", mt.Uint32())
		if i%5 == 4 {
			fmt.Fprintln(&b)
		}
	}
	fmt.Fprintln(&b)
	fmt.Fprintln(&b, "1000 outputs of genrand_real2()")
	for i := 0; i < 1000; i++ {
		fmt.Fprintf(&b, "%10.8f ", mt.Real2())
		if i%5 == 4 {
			fmt.Fprintln(&b)
		}
	}

	buf := b.Bytes()

	original, err := ioutil.ReadFile("mt19937ar.out")
	if err != nil {
		t.Fatal(err)
	}

	if !bytes.Equal(buf, original) {
		t.Fatal("not equal result")
	}

	err = ioutil.WriteFile("mt19937ar-go.out", buf, 0666)
	if err != nil {
		t.Fatal(err)
	}
}
