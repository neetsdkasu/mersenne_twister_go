/* Mersenne Twister 移植
 * Leonardone @ NEETSDKASU
 * BSD-2-Clause License
 */

package mersenne_twister_go

const (
	n           = 624
	m           = 397
	matrixA     = 0x9908b0df
	upperMask   = 0x80000000
	lowerMask   = 0x7fffffff
	DefaultSeed = 5489
)

var mag01 = [2]uint32{0x0, matrixA}

type MersenneTwister struct {
	mt  [n]uint32
	mti int
}

func NewMersenneTwister() *MersenneTwister {
	return new(MersenneTwister).InitByDefaultSeed()
}

func (self *MersenneTwister) InitByDefaultSeed() *MersenneTwister {
	return self.Init(DefaultSeed)
}

/* initializes mt[N] with a seed */
func (self *MersenneTwister) Init(seed uint32) *MersenneTwister {
	mt := self.mt[:]
	mt[0] = seed
	for i := 1; i < n; i++ {
		mt[i] = 1812433253*(mt[i-1]^(mt[i-1]>>30)) + uint32(i)
	}
	self.mti = n
	return self
}

/* initialize by an array */
func (self *MersenneTwister) InitByArray(key []uint32) *MersenneTwister {
	self.Init(19650218)
	mt := self.mt[:]
	i, j, k := 1, 0, len(key)
	if k < n {
		k = n
	}
	for ; k > 0; k-- {
		mt[i] = (mt[i] ^ ((mt[i-1] ^ (mt[i-1] >> 30)) * 1664525)) + key[j] + uint32(j)
		i++
		if i >= n {
			mt[0] = mt[n-1]
			i = 1
		}
		j++
		if j >= len(key) {
			j = 0
		}
	}
	for k = n - 1; k > 0; k-- {
		mt[i] = (mt[i] ^ ((mt[i-1] ^ (mt[i-1] >> 30)) * 1566083941)) - uint32(i)
		i++
		if i >= n {
			mt[0] = mt[n-1]
			i = 1
		}
	}
	mt[0] = 0x80000000
	return self
}

/* generates a random number on [0,0xffffffff]-interval */
func (self *MersenneTwister) Uint32() uint32 {
	var y uint32

	if self.mti >= n { /* generate N words at one time */
		var kk int
		mt := self.mt[:]
		for kk = 0; kk < n-m; kk++ {
			y = (mt[kk] & upperMask) | (mt[kk+1] & lowerMask)
			mt[kk] = mt[kk+m] ^ (y >> 1) ^ mag01[y&0x1]
		}
		for ; kk < n-1; kk++ {
			y = (mt[kk] & upperMask) | (mt[kk+1] & lowerMask)
			mt[kk] = mt[kk+(m-n)] ^ (y >> 1) ^ mag01[y&0x1]
		}
		y = (mt[n-1] & upperMask) | (mt[0] & lowerMask)
		mt[n-1] = mt[m-1] ^ (y >> 1) ^ mag01[y&0x1]
		self.mti = 0
	}

	y = self.mt[self.mti]
	self.mti++

	/* Tempering */
	y ^= y >> 11
	y ^= (y << 7) & 0x9d2c5680
	y ^= (y << 15) & 0xefc60000
	y ^= y >> 18

	return y
}

/* generates a random number on [0,0x7fffffff]-interval */
func (self *MersenneTwister) Int31() int32 {
	return int32(self.Uint32() >> 1)
}

/* generates a random number on [0,1]-real-interval */
func (self *MersenneTwister) Real1() float64 {
	return float64(self.Uint32()) * (1.0 / 4294967295.0)
	/* divided by 2^32-1 */
}

/* generates a random number on [0,1)-real-interval */
func (self *MersenneTwister) Real2() float64 {
	return float64(self.Uint32()) * (1.0 / 4294967296.0)
	/* divided by 2^32 */
}

/* generates a random number on (0,1)-real-interval */
func (self *MersenneTwister) Real3() float64 {
	return (float64(self.Uint32()) + 0.5) * (1.0 / 4294967296.0)
	/* divided by 2^32 */
}

/* generates a random number on [0,1) with 53-bit resolution*/
func (self *MersenneTwister) Res53() float64 {
	a := self.Uint32() >> 5
	b := self.Uint32() >> 6
	return (float64(a)*67108864.0 + float64(b)) * (1.0 / 9007199254740992.0)
}

/*
 *  疑似乱数生成機(RNG)  移植(Porting)
 *  Information of Original Source
 *  Mersenne Twister with improved initialization (2002)
 *  http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/mt.html
 *  http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/MT2002/mt19937ar.html
 */
// = 移植元ラインセンス (License of Original Source) =======================================================
// ======================================================================
/*
   A C-program for MT19937, with initialization improved 2002/1/26.
   Coded by Takuji Nishimura and Makoto Matsumoto.

   Before using, initialize the state by using init_genrand(seed)
   or init_by_array(init_key, key_length).

   Copyright (C) 1997 - 2002, Makoto Matsumoto and Takuji Nishimura,
   All rights reserved.

   Redistribution and use in source and binary forms, with or without
   modification, are permitted provided that the following conditions
   are met:

     1. Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.

     2. Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

     3. The names of its contributors may not be used to endorse or promote
        products derived from this software without specific prior written
        permission.

   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
   "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
   LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
   A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
   PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
   LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
   NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
   SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


   Any feedback is very welcome.
   http://www.math.sci.hiroshima-u.ac.jp/~m-mat/MT/emt.html
   email: m-mat @ math.sci.hiroshima-u.ac.jp (remove space)
*/
// ======================================================================
